PAGAZZI Lighting was founded in 1980. You won’t find more choice or more helpful, perceptive advice anywhere else. We are passionate about lighting and all that it can achieve. Our buyers travel nationally and internationally to find designs that extend and enhance our selections.

Address: Boulevard Retail Park, Links Road, Aberdeen, Aberdeenshire AB11 5EJ, UK

Phone: +44 1224 252333

Website: https://www.pagazzi.com/aberdeen-lighting-superstore/
